

function createModel(nGPU)

​

 

   local la1 = nn.Concat(2)

   ds = 3

   local noda =nn.Sequential()

     noda:add(nn.SpatialConvolutionSF(1,3,32,3,3,ds,ds))

     noda:add(nn.ReLU(true))

     noda:add(nn.SpatialMaxPooling(3,3,2,2)) 

​

	local nodb =nn.Sequential()

     nodb:add(nn.SpatialConvolutionSF(2,3,32,5,5,ds,ds,2,2))

     nodb:add(nn.ReLU(true))

     nodb:add(nn.SpatialMaxPooling(3,3,2,2)) 

​

	local nodc =nn.Sequential()

     nodc:add(nn.SpatialConvolutionSF(3,3,32,7,7,ds,ds,3,3))

     nodc:add(nn.ReLU(true))

     nodc:add(nn.SpatialMaxPooling(3,3,2,2)) 

​

	local nodd =nn.Sequential()

     nodd:add(nn.SpatialConvolutionSF(4,3,32,11,11,ds,ds,5,5))

     nodd:add(nn.ReLU(true))

     nodd:add(nn.SpatialMaxPooling(3,3,2,2)) 

 

	la1:add(noda):add(nodb):add(nodc):add(nodd)

​

   

​

​

​

   local features = nn.Concat(2)

   local fb1 = nn.Sequential() -- branch 1

 --  fb1:add(nn.SpatialConvolution(3,48,11,11,4,4,2,2))       -- 224 -> 55

 --  fb1:add(nn.ReLU(true))

 --  fb1:add(nn.SpatialMaxPooling(3,3,2,2))                   -- 55 ->  27

   fb1:add(nn.SpatialConvolution(128,128,5,5,1,1,2,2))       --  27 -> 27

   fb1:add(nn.ReLU(true))

   fb1:add(nn.SpatialMaxPooling(3,3,2,2))                   --  27 ->  13

   fb1:add(nn.SpatialConvolution(128,192,3,3,1,1,1,1))      --  13 ->  13

   fb1:add(nn.ReLU(true))

   fb1:add(nn.SpatialConvolution(192,192,3,3,1,1,1,1))      --  13 ->  13

   fb1:add(nn.ReLU(true))

   fb1:add(nn.SpatialConvolution(192,128,3,3,1,1,1,1))      --  13 ->  13

   fb1:add(nn.ReLU(true))

   fb1:add(nn.SpatialMaxPooling(3,3,2,2))                   -- 13 -> 6

​

   local fb2 = fb1:clone() -- branch 2

   -- for k,v in ipairs(fb2:findModules('nn.SpatialConvolution')) do

   --    v:reset() -- reset branch 2's weights

   -- end

​

   features:add(fb1)

   features:add(fb2)

   features:cuda()

   features = makeDataParallel(features, nGPU) -- defined in util.lua

​

   -- 1.3. Create Classifier (fully connected layers)

   local classifier = nn.Sequential()

   classifier:add(nn.View(256*9*9))

   classifier:add(nn.Dropout(0.5))

   classifier:add(nn.Linear(256*9*9, 4096))

   classifier:add(nn.Threshold(0, 1e-6))

   classifier:add(nn.Dropout(0.5))

   classifier:add(nn.Linear(4096, 4096))

   classifier:add(nn.Threshold(0, 1e-6))

   classifier:add(nn.Linear(4096, nClasses))

   classifier:add(nn.LogSoftMax())

   classifier:cuda()

​

   -- 1.4. Combine 1.1 and 1.3 to produce final model

   local model = nn.Sequential():add(la1):add(features):add(classifier)

   model.imageSize = 256

   model.imageCrop = 224

​

   -- Test forward pass:

​

   -- local input = torch.Tensor(1,3,256,256)

   -- local o = input

​

   -- for i=1,#model.modules do

   --   if i==1 then

   --    for k=1,#model.modules[i].modules do

   --      --print(model.modules[i].modules[k])

   --      ok = model.modules[i].modules[k]:forward(o)

   --      --print(#ok)

   --    end

   --   end

   --   --print(model.modules[i])

   --   o = model.modules[i]:forward(o)

   --   --print(#o)

   -- end

​

​

   return model

end



